import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Answer {
    public static void main(String args[]) {
        fileSort("randomNumbers.txt");
        factorial(20);
    }

    private static void fileSort(String fileName) {
        String toSort[];
        List<String> lines = null;
        try {
            lines = Files.readAllLines(Paths.get("src/main/resources/" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        toSort = lines.get(0).split(",");
        Arrays.sort(toSort, Comparator.comparingInt(Integer::parseInt));
        System.out.print("Sort:");
        System.out.println(Arrays.asList(toSort));
        Arrays.sort(toSort, (a, b)-> Integer.parseInt(b) - Integer.parseInt(a));
        System.out.print("Reverse Sort:");
        System.out.println(Arrays.asList(toSort));
    }

    private static void factorial(int factorial) {
        long ans = 1;
        for (int i = 1; i <= factorial; i++) {
            ans = ans * i;
        }
        System.out.print("Factorial: ");
        System.out.println(ans);
    }
}
